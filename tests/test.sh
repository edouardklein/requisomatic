#!/bin/bash
# Monolithic test of all of requisomatic functionality
set -e
set -u
set -x
set -o pipefail
#############
# ######### #
# # setup # #
# ######### #
#############
TEST_DIR=$(dirname "$(realpath "$0")")
pushd "$TEST_DIR/.."

TMP_DIR=$(mktemp -d --tmpdir requisomaticDB-XXX)
# Start the server
env FLASK_APP=requisomatic.py REQUISOMATIC_DB_FILE="${TMP_DIR}/db.sqlite3" python3 -m flask run&
PID=$!
# Kill the server when we exit
cleanup(){
    kill -9 $PID
    rm -rf "${TMP_DIR}"
}
trap cleanup EXIT
popd
echo 'Starting the server...'
counter=0
while [ $counter -lt 100 ]
do
    # poll the server until it answers
    sleep .1
    curl --fail --silent --show-error  http://127.0.0.1:5000/version | grep -E "^[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+" && break
    counter=$((counter+1))
done
if [ $counter -eq 100 ]
then
    exit 1
fi
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
####################
# ################ #
# # IP addresses # #
# ################ #
####################
echo "Get the redirection URL..."
# It contains the UUID of our session
# https://stackoverflow.com/a/3077316
URL=$(curl --fail -Ls -o /dev/null -w %{url_effective} http://127.0.0.1:5000/)
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# We will visit all the states of the underlying state machine
#######
# Add #
#######
echo "Get current state page"
curl --fail --silent --show-error "$URL" |  grep "(state 'add)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"

#########
# Whois #
#########
echo "Add IP addresses..."
curl --fail --silent --show-error --location --form addresses='192.0.2.1  test net 1
Line with no IP
198.51.100.12  test net 2
203.0.113.22 test net 3
' "$URL"/add | grep "(state 'whois)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# Poll the server until all IPs have been whoised
counter=0
while [ $counter -lt 100 ]
do
    sleep 1
    curl --fail --silent --show-error  "$URL" > "${TMP_DIR}/toto.html"  # Can't do that in one line if
    grep progress "${TMP_DIR}/toto.html" || break  # I want to catch errors in the curl
    counter=$((counter+1))
done
if [ $counter -eq 100 ]
then
    exit 1
fi
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# All of our IPs should appear in the html:
curl --fail --silent --show-error --location "$URL" |  grep 192.0.2.1
curl --fail --silent --show-error --location "$URL" |  grep 198.51.100.12
curl --fail --silent --show-error --location "$URL" |  grep 203.0.113.22
echo -e "\e[42m \xE2\x9C\x94 \e[49m"

###########
# Contact #
###########
echo "Select Hosting providers..."
curl --fail --silent --show-error --location --form hosting_provider_192.0.2.1='toto.example.com' \
     --form hosting_provider_198.51.100.12='titi.example.com' \
     --form hosting_provider_203.0.113.22='_custom' --form custom_hosting_provider_203.0.113.22='titi.example.com' \
     "$URL"/whois/ip | grep -E "(state 'contact)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# All of our IPs should again appear in the html:
curl --fail --silent --show-error --location "$URL" |  grep 192.0.2.1
curl --fail --silent --show-error --location "$URL" |  grep 198.51.100.12
curl --fail --silent --show-error --location "$URL" |  grep 203.0.113.22
echo -e "\e[42m \xE2\x9C\x94 \e[49m"

############
# Template #
############
echo "Select points of contact..."
curl --fail --silent --show-error --location \
     --form contact_toto.example.com='_custom' \
     --form custom_contact_toto.example.com='abuse@toto.example.com' \
     --form contact_titi.example.com='titi.example.com_0' \
     "$URL"/contact | grep "(state 'template)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# All of our IPs should still appear in the html:
curl --fail --silent --show-error --location "$URL" |  grep 192.0.2.1
curl --fail --silent --show-error --location "$URL" |  grep 198.51.100.12
curl --fail --silent --show-error --location "$URL" |  grep 203.0.113.22
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# Add a new template
echo "Add a new template..."
curl --fail --silent --show-error --form new_template_name='test_template' \
     --form new_template_file="@${TEST_DIR}/template.odt" \
     "$URL"/new_template
echo -e "\e[42m \xE2\x9C\x94 \e[49m"

############
# Download #
############
# Use that template as well as the default one
echo "Select the templates..."
curl --fail --silent --show-error --location \
     --form template_toto.example.com='défaut' \
     --form template_titi.example.com='test_template' \
     "$URL"/template | grep "(state 'download)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# All of our IPs should finally appear in the html:
curl --fail --silent --show-error --location "$URL" |  grep 192.0.2.1
curl --fail --silent --show-error --location "$URL" |  grep 198.51.100.12
curl --fail --silent --show-error --location "$URL" |  grep 203.0.113.22
echo "Download one subpoena..."
curl --fail  --silent --show-error "$URL"/download/toto.example.com.odt --output /dev/null
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
echo "Download one subpoena..."
curl --fail  --silent --show-error "$URL"/download/titi.example.com.odt --output /dev/null
echo -e "\e[42m \xE2\x9C\x94 \e[49m"


#######################
# ################### #
# # Email addresses # #
# ################### #
#######################
echo "Get the redirection URL..."
# It contains the UUID of our session
# https://stackoverflow.com/a/3077316
URL=$(curl --fail -Ls -o /dev/null -w %{url_effective} http://127.0.0.1:5000/)
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# We will visit all the states of the underlying state machine
#######
# Add #
#######
echo "Get current state page"
curl --fail --silent --show-error "$URL" |  grep "(state 'add)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"

#########
# Whois #
#########
echo "Add email addresses..."
curl --fail --silent --show-error --location --form addresses='alice@example.com Some comment
bob@titi.example.com Some other comment
charlie@tata.example.com Wazaaaa
no addresses on this line
' "$URL"/add | grep "(state 'whois)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# All of our domains should appear in the html:
curl --fail --silent --show-error --location "$URL" |  grep example.com
curl --fail --silent --show-error --location "$URL" |  grep titi.example.com
curl --fail --silent --show-error --location "$URL" |  grep tata.example.com
echo -e "\e[42m \xE2\x9C\x94 \e[49m"

###########
# Contact #
###########
echo "Select Hosting providers..."
curl --fail --silent --show-error --location \
     --form hosting_provider_example.com='example.com' \
     --form hosting_provider_titi.example.com='titi.example.com' \
     --form hosting_provider_tata.example.com='_custom' --form custom_hosting_provider_tata.example.com='example.com' \
     "$URL"/whois/email | grep -E "(state 'contact)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# All of our domains should again appear in the html
curl --fail --silent --show-error --location "$URL" |  grep example.com
curl --fail --silent --show-error --location "$URL" |  grep titi.example.com
curl --fail --silent --show-error --location "$URL" |  grep tata.example.com

############
# Template #
############
echo "Select points of contact..."
curl --fail --silent --show-error --location \
     --form contact_example.com='_custom' \
     --form custom_contact_example.com='abuse@example.com' \
     --form contact_titi.example.com='titi.example.com_0' \
     "$URL"/contact | grep "(state 'template)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# All of our domains should still appear in the html
curl --fail --silent --show-error --location "$URL" |  grep example.com
curl --fail --silent --show-error --location "$URL" |  grep titi.example.com
curl --fail --silent --show-error --location "$URL" |  grep tata.example.com
# Add a new template
echo "Add a new template..."
curl --fail --silent --show-error --form new_template_name='test_template' \
     --form new_template_file="@${TEST_DIR}/template.odt" \
     "$URL"/new_template
echo -e "\e[42m \xE2\x9C\x94 \e[49m"

############
# Download #
############
# Use that template as well as the default one
echo "Select the templates..."
curl --fail --silent --show-error --location \
     --form template_example.com='défaut' \
     --form template_titi.example.com='test_template' \
     "$URL"/template | grep "(state 'download)"
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
# All of our domains should finally appear in the html
curl --fail --silent --show-error --location "$URL" |  grep example.com
curl --fail --silent --show-error --location "$URL" |  grep titi.example.com
curl --fail --silent --show-error --location "$URL" |  grep tata.example.com
echo "Download one subpoena..."
curl --fail  --silent --show-error "$URL"/download/example.com.odt --output /dev/null
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
echo "Download one subpoena..."
curl --fail  --silent --show-error "$URL"/download/titi.example.com.odt --output /dev/null
echo -e "\e[42m \xE2\x9C\x94 \e[49m"
