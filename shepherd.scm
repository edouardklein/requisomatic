(load "guix.scm")
(use-modules
 (gnu)
 (guix packages)
 (guix build-system trivial)
 (beaverlabs packages requisomatic)
 (gnu packages python-web)
 (gnu packages bash)
 (gnu services web)
 (ice-9 pretty-print)
 (srfi srfi-9)
 (srfi srfi-9 gnu))

(define minimal
  (operating-system
    (host-name "Requisomatic")
    (timezone "Europe/Paris")
    (locale "en_US.utf8")
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)))
    (file-systems %base-file-systems)
    (users %base-user-accounts)
    (packages %base-packages)
    (services %base-services)))

(define* (nginx-http-reverse-proxy os #:key
                             (dest-port 8000)
                             (source-port 80))
  (set-fields os
              ((operating-system-user-services)
               (cons
                (service
                 nginx-service-type
                 (nginx-configuration
                  (server-blocks
                   (list (nginx-server-configuration
                          (listen `(,(format #f "~a" source-port)))
                          (server-name '("localhost"))
                          (locations
                           (list
                            (nginx-location-configuration
                             (uri "/")
                             (body `(,(format #f "proxy_pass http://localhost:~a;"
                                          dest-port)))))))))))
                        ;;  )))))
                (operating-system-user-services os)))))
                            

; (nginx-reverse-proxy minimal #:src-host "localhost" #:src-port 80 #:dest-host "127.0.0.1" #:dest-port 11223)

(nginx-http-reverse-proxy (requisomatic-webapp minimal #:port 11223)
                          #:dest-port 11223)
