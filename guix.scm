(define-module (beaverlabs packages requisomatic)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix modules)
  #:use-module (gnu packages)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system shadow)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix profiles)
  #:use-module (guix records)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system python)
  #:use-module (guix licenses)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages curl)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (requisomatic-webapp)
  )

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f2" OPEN_READ)))

(define (skip-git-and-build-directory file stat)
   "Skip the `.git` and `build` and `guix_profile` directory when collecting the sources."
  (let ((name (basename file)))
    (not (or (string=? name ".git")
             (string=? name "build")
             (string-prefix? "guix_profile" name)))))

(define-public *version* (call-with-input-file "VERSION"
                    get-string-all))

(define-public python-ipaddr
  (package
    (name "python-ipaddr")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "ipaddr" version))
        (sha256
          (base32
            "1ml8r8z3f0mnn381qs1snbffa920i9ycp6mm2am1d3aqczkdz4j0"))))
    (build-system python-build-system)
    (home-page "https://github.com/google/ipaddr-py")
    (synopsis
      "Google's IP address manipulation library")
    (description
      "Google's IP address manipulation library")
    (license license:asl2.0)))

(define-public python-ipwhois
  (package
    (name "python-ipwhois")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "ipwhois" version))
        (sha256
          (base32
            "1gjbr2bbarhz47nk8kqpnz4cci8gp3imd54f0hd20hc07gpky7l3"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-dnspython" ,python-dnspython-1.16)
        ("python-ipaddr" ,python-ipaddr)))
    (home-page "https://github.com/secynic/ipwhois")
    (synopsis
      "Retrieve and parse whois data for IPv4 and IPv6 addresses.")
    (description
      "Retrieve and parse whois data for IPv4 and IPv6 addresses.")
    (license license:bsd-3)))

(define-public python-markdown2
  (package
    (name "python-markdown2")
    (version "2.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "markdown2" version))
        (sha256
          (base32
            "06wvhai41in8xdvwmn97d6d0fyvlhsqyj0agd27zdrj4wpq6kmr8"))))
    (build-system python-build-system)
    (home-page
      "https://github.com/trentm/python-markdown2")
    (synopsis
      "A fast and complete Python implementation of Markdown")
    (description
      "A fast and complete Python implementation of Markdown")
    (license license:expat)))

(define-public python-secretary
  (package
    (name "python-secretary")
    (version "0.2.19")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "secretary" version))
        (sha256
          (base32
            "1llclidh3p4z199zzrhgp8b14v29iyai6h6n8bp1p20y9d2zpy4h"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-jinja2" ,python-jinja2)
        ("python-markdown2" ,python-markdown2)))
        (arguments
         `(#:phases
           (modify-phases %standard-phases
                          (delete 'check))))

    (home-page
      "https://github.com/christopher-ramirez/secretary")
    (synopsis
      "Take the power of Jinja2 templates to OpenOffice or LibreOffice.")
    (description
      "Take the power of Jinja2 templates to OpenOffice or LibreOffice.")
    (license license:expat)))

(define-public requisomatic
  (package
   (name "requisomatic")
   (version (git-version *version* "HEAD" %git-commit))
   (source (local-file %source-dir
                       #:recursive? #t
                       #:select? skip-git-and-build-directory))
   (propagated-inputs `(
             ("python" ,python)
             ("python-flask" ,python-flask)
             ("python-ipwhois" ,python-ipwhois)
             ("python-pandas" ,python-pandas)
             ("python-pygments" ,python-pygments)
             ("python-secretary" ,python-secretary)
             ("curl" ,curl)
             ))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan '(
                       ("./" "bin/requisomatic")
                       )
      #:phases
      (modify-phases %standard-phases
                     (add-before
                      'build 'check
                      (lambda* (#:key outputs #:allow-other-keys)
                               (let* ((dir (string-append
                                            (assoc-ref outputs "out")
                                            "/bin/requisomatic")))
                                 (chdir dir)
                                 (invoke "make" "test")
                                 #t))))
   ))
   (synopsis "Quickly edit multiple subpoenas")
   (description
    "Give IP, get supboenas.")
   (home-page "https://gitlab.com/edouardklein/requisomatic")
   (license license:agpl3+)))

;; (define (wrap-in-search-paths (exec packages))
;;   "Return a gexp that wraps EXEC in a script that sets the environment variables from the given PACKAGES' search paths.

;; Typically used to wrap a package's executables so that they can find their non propagated inputs."
;;   (let ((search-paths (apply append (map package-search-paths packages))))
;;     )

(define serviceable-requisomatic
  (package
   (name "serviceable-requisomatic")
   (version *version*)
   (source #f)
   (propagated-inputs
    `(("requisomatic" ,requisomatic)
      ("gunicorn" ,gunicorn)
      ("bash" ,bash)))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((bash (assoc-ref %build-inputs "bash"))
               (requisomatic (assoc-ref %build-inputs "requisomatic"))
               (dir (string-append (assoc-ref %outputs "out") "/bin"))
               (fname (string-append dir "/requisomatic-server")))
          (mkdir-p  dir)
          (with-output-to-file fname
            (lambda _
              (display (string-append "#!" bash "/bin/bash\n"))
              (display "source /run/current-system/profile/etc/profile\n")
              (display "REQUISOMATIC_DB_FILE=$1 gunicorn --bind=$2 --pid=$3 requisomatic:app\n")))
          (chmod fname #o755)
          #t))))
   (synopsis "A custom package to run requisomatic as a server")
   (description #f)
   (license #f)
   (home-page #f)))

(define-record-type* <requisomatic-configuration>
  requisomatic-configuration make-requisomatic-configuration
  requisomatic-configuation?
  (user          requisomatic-configuration-user
                 (default "requisomatic"))         ;string
  (group         requisomatic-configuration-group         ;string
                 (default "requisomatic"))
  (db-file       requisomatic-db-file   ; path
                 (default "/var/lib/requisomatic/db.sqlite3"))
  (log-file      requisomatic-log-file ;path
                 (default "/var/log/requisomatic.log"))
  (bind-to       requisomatic-bind-to  ; https://docs.gunicorn.org/en/stable/run.html#commonly-used-arguments
                 ;; see the --bind option of gunicorn
                 (default "127.0.0.1:8000"))
  (pid-file      requisomatic-pid-file ;path
                 (default "/var/run/requisomatic/gunicorn.pid")))

(define requisomatic-shepherd
  (match-lambda
   (($ <requisomatic-configuration> user group db-file log-file bind-to pid-file)
    (with-imported-modules (source-module-closure
                            '((guix build utils)
                              (gnu build shepherd)
                              (gnu system file-systems)))
      (list (shepherd-service
             (provision '(requisomatic))
             (requirement '(user-processes networking))
             (documentation (string-append "Run the requisomatic web server with gunicorn"))
             (auto-start? #t)
             (start #~(begin
                        (format #t "coucou************************************\n")
                        (use-modules (guix build utils)
                                     (gnu build shepherd)
                                     (gnu system file-systems))
                        (make-forkexec-constructor/container
                         '(#$(file-append serviceable-requisomatic "/bin/requisomatic-server") #$db-file #$bind-to #$pid-file)
                         #:directory (string-append #$requisomatic "/bin/requisomatic/")
                         #:pid-file #$pid-file
                         #:log-file #$log-file
                         #:user #$user
                         #:group #$group
                         ;; #:mappings `(
                         ;;             (file-system-mapping
                         ;;              (source ,(dirname #$pid-file))
                         ;;              (target source)
                         ;;              (writable? #t))
                         ;;             (file-system-mapping
                         ;;              (source ,(dirname #$log-file))
                         ;;              (target source)
                         ;;              (writable? #t))
                         ;;             (file-system-mapping
                         ;;              (source ,(dirname $db-file))
                         ;;              (target source)
                         ;;              (writable? #t)))
                         )))
             (stop #~(make-kill-destructor))))))))

(define requisomatic-activation
  (match-lambda
   (($ <requisomatic-configuration> user group db-file log-file bind-to pid-file)
    (with-imported-modules '((guix build utils))
      #~(begin
           (use-modules (guix build utils))
           (let ((db-dir  (dirname #$db-file))
                 (pid-dir (dirname #$pid-file)))
             (mkdir-p db-dir)
             (chown db-dir (passwd:uid (getpwnam #$user)) (group:gid (getgrnam #$group)))
             (mkdir-p pid-dir)
             (chown pid-dir (passwd:uid (getpwnam #$user)) (group:gid (getgrnam #$group)))))))))

(define requisomatic-account
  (match-lambda
   (($ <requisomatic-configuration> user group db-file log-file bind-to pid-file)
    (list
     (user-group
      (name group))
     (user-account
      (name user)
      (group group))))))

(define requisomatic-service-type
  (service-type (name 'requisomatic)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          requisomatic-shepherd)
                       (service-extension activation-service-type
                                          requisomatic-activation)
                       (service-extension account-service-type
                                          requisomatic-account)
                       (service-extension profile-service-type
                                          (const (list serviceable-requisomatic)))))
                (default-value (requisomatic-configuration))
                (description "Run the requisomatic web application with gunicorn.")))

(define* (requisomatic-webapp os #:key
                                     (user "requisomatic")
                                     (group "requisomatic")
                                     (pid-file "/var/run/requisomatic/gunicorn.pid")
                                     (db-file "/var/lib/requisomatic/db.sqlite3")
                                     (port 8000)
                                     (ip "127.0.0.1")
                                     (log-file "/var/log/requisomatic.log"))
  (set-fields os
              ((operating-system-user-services) (cons (service requisomatic-service-type
                                                             (requisomatic-configuration
                                                              (user user)
                                                              (group group)
                                                              (db-file db-file)
                                                              (log-file log-file)
                                                              (pid-file pid-file)
                                                              (bind-to (format #f "~a:~a" ip port))))
                                                 (operating-system-user-services os)))))

requisomatic
