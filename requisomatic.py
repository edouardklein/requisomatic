import os
from flask import Flask, render_template, redirect, Markup, request, Response
import sqlite3
import uuid
from threading import Lock, Thread
import time
import re
import random
import json
import datetime
import ipwhois
import pandas as pd
import pprint
import sys
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import HtmlFormatter
import tempfile
from werkzeug.utils import secure_filename
from secretary import Renderer

app = Flask(__name__)
secretary_engine = Renderer()
__version__ = open("VERSION").read()
#: The path to the temporary directory where to store templates and subpoenas
TMPDIR = tempfile.mkdtemp(prefix="requisomatic")
print(f"TMPDIR is {TMPDIR}")

#: The path to the sqlite database file
db_path = os.environ.get(
    "REQUISOMATIC_DB_FILE", default="/var/lib/requisomatic/db.sqlite3"
)
os.makedirs(os.path.dirname(db_path), exist_ok=True)
#: Number of second to wait between two consecutive whois queries
whois_delay = 10
db = sqlite3.connect(db_path, check_same_thread=False)
db_lock = Lock()
# This table stores the state a session is in, so that the client does not have
# to keep track of state, any request on the correct uuid will allow a user to
# resume their session
db.execute("CREATE TABLE IF NOT EXISTS state (uuid TEXT PRIMARY KEY, state TEXT);")
# This table keeps track, for a given session, of the list of ip addresses a
# user wants to get subpoenas for
db.execute(
    "CREATE TABLE IF NOT EXISTS ip_addresses (uuid TEXT, ip TEXT, comment TEXT, hosting_provider TEXT, PRIMARY KEY (uuid, ip));"
)
# This table keeps track, for a given session, of the list of email addresses a
# user wants to get subpoenas for
db.execute(
    "CREATE TABLE IF NOT EXISTS email_addresses (uuid TEXT, email TEXT, comment TEXT, hosting_provider TEXT, PRIMARY KEY (uuid, email));"
)
# This table keeps track, for a given session, of the contact information to use
# for each hosting provider
db.execute(
    "CREATE TABLE IF NOT EXISTS provider_contact (uuid TEXT, hosting_provider TEXT, contact TEXT, PRIMARY KEY (uuid, hosting_provider));"
)
# This table is global to all the users, it caches the whois information we fetch online
# The date field exist because maybe in the future we will provide historical whois search.
# For now, there is only one possible record per IP
db.execute(
    "CREATE TABLE IF NOT EXISTS whois (ip TEXT, whois TEXT, date TEXT, PRIMARY KEY (ip));"
)
# This table is global to all the users, it caches the contact information for the hosting providers
db.execute(
    "CREATE TABLE IF NOT EXISTS address_book (contact_id TEXT PRIMARY KEY, hosting_provider TEXT, contact TEXT);"
)
# Add some data we will use for the tests
db.execute(
    """INSERT INTO address_book (contact_id, hosting_provider, contact) VALUES (?, ?, ?)
ON CONFLICT(contact_id) DO NOTHING;""",
    ("titi.example.com_0", "titi.example.com", "legal@titi.example.com"),
)
db.execute(
    """INSERT INTO address_book (contact_id, hosting_provider, contact) VALUES (?, ?, ?)
ON CONFLICT(contact_id) DO NOTHING;""",
    ("titi.example.com_1", "titi.example.com", "abuse@titi.example.com"),
)
db.execute(
    """INSERT INTO address_book (contact_id, hosting_provider, contact) VALUES (?, ?, ?)
ON CONFLICT(contact_id) DO NOTHING;""",
    ("toto.example.com_0", "toto.example.com", "legal@toto.example.com"),
)
db.execute(
    """INSERT INTO address_book (contact_id, hosting_provider, contact) VALUES (?, ?, ?)
ON CONFLICT(contact_id) DO NOTHING;""",
    ("example.com_0", "example.com", "legal@example.com"),
)
# This table keeps track, for a given session, of which template to use for which hosting provider
db.execute(
    """CREATE TABLE IF NOT EXISTS provider_template (uuid TEXT, hosting_provider TEXT, template_id TEXT, PRIMARY KEY (uuid, hosting_provider));"""
)
# And this one keeps track, for a given session, of all the available templates
db.execute(
    """CREATE TABLE IF NOT EXISTS template (uuid TEXT, template_id TEXT, template_path TEXT, PRIMARY KEY (uuid, template_id));"""
)
db.commit()


def sql1ao1(s, *args):
    """Return the results of the given query, which is supposed to return one and only one element.

    If the returned row has only one element, this element is extracted from
    the row and returned directly, instead of returning a singleton list.

    """
    answer = db.execute(s, args).fetchall()
    assert (
        len(answer) == 1
    ), f"Query {s}, {args} returned 0 or more than one row: {answer}"
    row = answer[0]
    if len(row) == 1:
        return row[0]
    return row


def sql1(s, *args):
    """Return the results of the given query, which is supposed to return at most one element.

    If the returned row has only one element, this element is extracted from
    the row and returned directly, instead of returning a singleton list.

    """
    answer = db.execute(s, args).fetchall()
    assert (
        len(answer) == 0 or len(answer) == 1
    ), f"Query {s}, {args} returned more than one row: {answer}"
    if len(answer) == 0:
        return None
    row = answer[0]
    if len(row) == 1:
        return row[0]
    return row


def sqll(s, *args):
    """Return the results of the given query, as a list.

    If the returned rows have only one element, this element is extracted from
    the rows and returned directly as a list of elements, instead of returning
    a list of singleton lists.

    """
    answer = db.execute(s, args).fetchall()
    if len(answer) == 0:
        return []
    row = answer[0]
    if len(row) == 1:
        return [row[0] for row in answer]
    return answer


def sql_commit(q, *args, nolock=False):
    """Execute the given query and commit. Use nolock if the lock was acquired before the call."""
    if not nolock:
        with db_lock:
            db.execute(q, args)
            db.commit()
    else:
        db.execute(q, args)
        db.commit()


def whois_lookup(addr):
    """Acutally run the query for the given address, and store the returned dict in
    the database. Return True if a query was actually made to a server, False
    otherwise (this allows the caller to forego the minimal wait time between
    requests if no request was made).

    """

    def commit_whois(obj):
        """Commit the given obj as the whois info for addr"""
        sql_commit(
            """UPDATE whois SET whois = ?, date = ? WHERE ip = ?;""",
            json.dumps(obj),
            datetime.datetime.now().isoformat(),
            addr,
            nolock=True,
        )

    try:
        ipwobj = ipwhois.IPWhois(addr)
        commit_whois(ipwobj.lookup_rdap())
        return True
    except ipwhois.exceptions.IPDefinedError:
        commit_whois(
            {"Error": "This IP belongs to nobody, it is a private or test IP."}
        )
        return False
    except Exception as e:
        commit_whois(
            {
                "Error": f"The following error was encountered while trying to get the whois info for this IP:\n{e}"
            }
        )
        return False


def whois_loop():
    """Select all the unwhoised IP addresses in the database, and whois one of them, in a loop"""
    nosleep = False
    while True:
        with db_lock:
            ip_addresses = sqll("SELECT ip FROM whois WHERE whois IS NULL;")
            if len(ip_addresses) > 0:
                whois_lookup(random.choice(ip_addresses)) or (nosleep := True)
        time.sleep(whois_delay) if not nosleep else (nosleep := False)


whois_daemon = Thread(target=whois_loop, daemon=True)
whois_daemon.start()


def breadcrumbs(state, uuid):
    """Return the HTML for the breadcrumbs for the current state"""
    return Markup(render_template("breadcrumbs.html", state=state, uuid=uuid))


@app.route("/version")
def version():
    """Return the current version"""
    return __version__


@app.route("/")
def slash():
    "Redirect to a random uuid"
    return redirect(f"/{uuid.uuid4()}", code=303)


def email_domain(email):
    """Return the domain of the given email address"""
    return email.split("@")[1]


def whois_email_table(uuid):
    """Return the HTML for the table in the whois state for the emails the given uuid is interested in."""
    email_addresses = sqll(
        """SELECT email FROM email_addresses WHERE uuid = ?;""", uuid
    )
    comments = [
        sql1(
            """SELECT comment FROM email_addresses WHERE email = ? AND uuid = ?;""",
            email,
            uuid,
        )
        for email in email_addresses
    ]
    domains = [email_domain(email) for email in email_addresses]
    df = pd.DataFrame(
        columns=["Emails", "Domaine", "Commentaires"],
        data=[
            [email, domain, comment]
            for email, domain, comment in zip(email_addresses, domains, comments)
        ],
    )
    df = (
        df.groupby(by="Domaine")
        .aggregate(lambda l: "<br/>".join(l))
        .assign(Hébergeur=lambda df: list(map(whois_email_radios, df.index)))
    )
    return Markup(df.to_html(escape=False))


def whois_ip_table(uuid):
    """Return the HTML for the table in the whois state for the ips the given uuid is interested in."""
    ip_addresses = sqll("""SELECT ip FROM ip_addresses WHERE uuid = ?;""", uuid)
    comments = [
        sql1(
            """SELECT comment FROM ip_addresses WHERE ip = ? AND uuid = ?;""", ip, uuid
        )
        for ip in ip_addresses
    ]
    whois_json_s = [
        sql1("""SELECT whois FROM whois WHERE ip = ?;""", ip) for ip in ip_addresses
    ]
    df = pd.DataFrame(
        columns=["Adresses IP", "Commentaire", "Hébergeur", "Données whois brutes"],
        data=[
            [ip, comment, whois_ip_radios(ip, s), f'<a href="/whois/{ip}">whois</a>']
            for ip, comment, s in zip(ip_addresses, comments, whois_json_s)
        ],
    )
    return Markup(df.to_html(escape=False))


email_res = r'[^" \\]+@[^" \\]+'
email_re = re.compile(email_res)


def whois_parse_emails(s):
    """Return the emails present in the given json string. FIXME: We could use a
    more formal way of extracting the information, but this does 90% of the job
    with 10% of the code.

    """
    return set(email_re.findall(s))


def whois_parse_domains(s):
    """Return the domains in the given json string. FIXME: we should find the owner
    of the IP in a more formal way, but this works well enough.

    """
    return set(e.split("@")[1] for e in whois_parse_emails(s))


def whois_ip_radio(ip, domain="_custom", checked=False):
    """Return the HTML markup for a single radio button"""
    answer = (
        f'<input type="radio" id="radio_{ip}_{domain}" name="hosting_provider_{ip}" '
        + ("checked" if checked else "")
        + f' value="{domain}">'
    )
    if domain != "_custom":
        answer += f'<label for="radio_{ip}_{domain}">{domain}</label>'
    else:
        answer += f'<label for="radio_{ip}__custom"><input type="text" name="custom_hosting_provider_{ip}"></input></label>'
    return answer


def whois_ip_radios(ip, s):
    """Return the HTML markup for a series of radio buttons to choose the hosting_provider (a domain name)
    for ip ip, with whois information s.

    """
    answer = ""
    checked = True
    whois_providers = whois_parse_domains(s)
    historical_providers = sqll(
        """SELECT hosting_provider FROM ip_addresses WHERE ip = ? AND hosting_provider IS NOT NULL;""",
        ip,
    )
    for domain in sorted(whois_providers.union(historical_providers)):
        answer += whois_ip_radio(ip, domain, checked=checked) + "<br/>"
        checked = False
    answer += whois_ip_radio(ip, checked=checked)
    return answer


def whois_email_radios(domain):
    """Return the HTML for a series of radio buttons to choose the hosting provider (hopefully a domain name) for a domain."""
    answer = ""
    checked = True
    historical_providers = sqll(
        """SELECT hosting_provider FROM email_addresses WHERE email LIKE ? and hosting_provider IS NOT NULL;""",
        f"%@{domain}",
    )
    for provider in sorted(set(historical_providers + [domain])):
        answer += (
            whois_ip_radio(domain, provider, checked=checked) + "<br/>"
        )  # We abuse whois_ip_radio here, but it does the job
        checked = False
    answer += whois_ip_radio(
        domain, checked=checked
    )  # Again, no need to write a whois_email_radio
    return answer


def contact_table(uuid):
    """Return the HTML for the table in the contact state for the given uuid."""
    ip_addresses = sqll("""SELECT ip FROM ip_addresses WHERE uuid = ?;""", uuid)
    comments = [
        sql1ao1(
            """SELECT comment FROM ip_addresses WHERE ip = ? AND uuid = ?;""", ip, uuid
        )
        for ip in ip_addresses
    ]
    hosting_providers = [
        sql1ao1(
            """SELECT hosting_provider FROM ip_addresses WHERE ip = ? AND uuid  = ?;""",
            ip,
            uuid,
        )
        for ip in ip_addresses
    ]
    email_addresses = sqll(
        """SELECT email FROM email_addresses WHERE uuid = ?;""", uuid
    )
    comments += [
        sql1ao1(
            """SELECT comment FROM email_addresses WHERE email = ? AND uuid = ?;""",
            email,
            uuid,
        )
        for email in email_addresses
    ]
    hosting_providers += [
        sql1ao1(
            """SELECT hosting_provider FROM email_addresses WHERE email = ? AND uuid = ?;""",
            email,
            uuid,
        )
        for email in email_addresses
    ]
    addresses = ip_addresses + email_addresses
    df = pd.DataFrame(
        columns=["Adresses IP et Email", "Commentaires", "Hébergeur"],
        data=[
            [address, comment, hosting_provider]
            for address, comment, hosting_provider in zip(
                addresses, comments, hosting_providers
            )
        ],
    )
    df = (
        df.groupby(by="Hébergeur")
        .aggregate(lambda l: "<br/>".join(l))
        .assign(Coordonnées=lambda df: list(map(contact_radios, df.index)))
    )
    return Markup(df.to_html(escape=False))


def contact_radios(hosting_provider):
    """Return the radio buttons for the known contact info for the given hosting provider."""
    answer = ""
    checked = True
    for contact_info, contact_id in sqll(
        """SELECT contact, contact_id FROM address_book WHERE hosting_provider = ?;""",
        hosting_provider,
    ):
        answer += (
            contact_radio(hosting_provider, contact_info, contact_id, checked=checked)
            + "<br/>"
        )
        checked = False
    answer += contact_radio(hosting_provider, checked=checked)
    return answer


def contact_radio(
    hosting_provider, contact_info="_custom", contact_id="_custom", checked=False
):
    """Return the HTML markup for a single contact radio button"""
    safe_contact_info = contact_info.replace("\n", "<br/>").replace("\r", "")
    answer = (
        f'<input type="radio" id="radio_{hosting_provider}" name="contact_{hosting_provider}" '
        + ("checked" if checked else "")
        + f' value="{contact_id}">'
    )
    if contact_info != "_custom":
        answer += f'<label for="radio_{hosting_provider}">{safe_contact_info}</label>'
    else:
        answer += f'<label for="radio_{hosting_provider}"><textarea name="custom_contact_{hosting_provider}" rows="5" class="smooth"></textarea></label>'
    return answer


def provider_contact(uuid, provider_name):
    """Return the chosen contact info HTML for the given provider"""
    return (
        sql1(
            """SELECT contact FROM provider_contact WHERE uuid=? AND hosting_provider = ?;""",
            uuid,
            provider_name,
        )
        .replace("\n", "<br/>")
        .replace("\r", "")
    )


def template_table(uuid):
    """Return the HTML for the table in the template state for the given uuid."""
    ip_addresses = sqll("""SELECT ip FROM ip_addresses WHERE uuid = ?;""", uuid)
    comments = [
        sql1ao1(
            """SELECT comment FROM ip_addresses WHERE ip = ? AND uuid = ?;""", ip, uuid
        )
        for ip in ip_addresses
    ]
    hosting_providers = [
        sql1ao1(
            """SELECT hosting_provider FROM ip_addresses WHERE ip = ? AND uuid  = ?;""",
            ip,
            uuid,
        )
        for ip in ip_addresses
    ]
    email_addresses = sqll(
        """SELECT email FROM email_addresses WHERE uuid = ?;""", uuid
    )
    comments += [
        sql1ao1(
            """SELECT comment FROM email_addresses WHERE email = ? AND uuid = ?;""",
            email,
            uuid,
        )
        for email in email_addresses
    ]
    hosting_providers += [
        sql1ao1(
            """SELECT hosting_provider FROM email_addresses WHERE email = ? AND uuid = ?;""",
            email,
            uuid,
        )
        for email in email_addresses
    ]
    addresses = ip_addresses + email_addresses
    df = pd.DataFrame(
        columns=["Adresses IP et email", "Commentaires", "Hébergeur"],
        data=[
            [address, comment, hosting_provider]
            for address, comment, hosting_provider in zip(
                addresses, comments, hosting_providers
            )
        ],
    )

    df = (
        df.groupby(by="Hébergeur")
        .aggregate(lambda l: "<br/>".join(l))
        .assign(
            Coordonnées=lambda df: list(
                map(lambda p: provider_contact(uuid, p), df.index)
            )
        )
        .assign(
            Modèle=lambda df: list(map(lambda p: template_dropdown(uuid, p), df.index))
        )
    )
    return Markup(df.to_html(escape=False))


def template_dropdown(uuid, provider_name):
    """Return the dropdown menus for the available templates for the given uuid."""
    answer = f"<select name=template_{provider_name}>"
    for template_id in sqll("""SELECT template_id FROM template WHERE uuid=?;""", uuid):
        answer += f"<option value={template_id}>{template_id}</option>"
    answer += "</select>"
    return answer


def download_table(uuid):
    """Return the HTML table in the download state for the given uuid."""
    # FIXME: This pattern repeats itself in the *_table functions, we should
    # probably factor it out
    ip_addresses = sqll("""SELECT ip FROM ip_addresses WHERE uuid = ?;""", uuid)
    comments = [
        sql1ao1(
            """SELECT comment FROM ip_addresses WHERE ip = ? AND uuid = ?;""", ip, uuid
        )
        for ip in ip_addresses
    ]
    hosting_providers = [
        sql1ao1(
            """SELECT hosting_provider FROM ip_addresses WHERE ip = ? AND uuid  = ?;""",
            ip,
            uuid,
        )
        for ip in ip_addresses
    ]
    email_addresses = sqll(
        """SELECT email FROM email_addresses WHERE uuid = ?;""", uuid
    )
    comments += [
        sql1ao1(
            """SELECT comment FROM email_addresses WHERE email = ? AND uuid = ?;""",
            email,
            uuid,
        )
        for email in email_addresses
    ]
    hosting_providers += [
        sql1ao1(
            """SELECT hosting_provider FROM email_addresses WHERE email = ? AND uuid = ?;""",
            email,
            uuid,
        )
        for email in email_addresses
    ]
    addresses = ip_addresses + email_addresses
    df = pd.DataFrame(
        columns=["Adresses IP et email", "Commentaire", "Hébergeur"],
        data=[
            [address, comment, hosting_provider]
            for address, comment, hosting_provider in zip(
                addresses, comments, hosting_providers
            )
        ],
    )

    def provider_template(uuid, provider_name):
        """Return the chosen template id for the given provider"""
        return sql1(
            """SELECT template_id FROM provider_template WHERE uuid=? AND hosting_provider=?;""",
            uuid,
            provider_name,
        )

    def provider_download(uuid, provider_name):
        """Return the download link for the given provider"""
        return f"<a href=/{uuid}/download/{provider_name}.odt>Télécharger</a>"

    df = (
        df.groupby(by="Hébergeur")
        .aggregate(lambda l: "<br/>".join(l))
        .assign(
            Coordonnées=lambda df: list(
                map(lambda p: provider_contact(uuid, p), df.index)
            )
        )
        .assign(
            Modèle=lambda df: list(map(lambda p: provider_template(uuid, p), df.index))
        )
        .assign(
            Téléchargement=lambda df: list(
                map(lambda p: provider_download(uuid, p), df.index)
            )
        )
    )
    return Markup(df.to_html(escape=False))


@app.route("/<uuid>")
def _uuid(uuid):
    """Render the template of the state the user is currently in.

    This is the only URL a user should see.

    POST and GET requests may happen on other endpoints, but they should
    redirect here, or trigger a download that would not direct the user away
    from this page.

    """
    state = sql1("SELECT state FROM state WHERE uuid = ?;", uuid)
    if state is None:
        sql_commit("INSERT INTO state VALUES (?, ?);", uuid, "add")
        sql_commit(
            "INSERT INTO template (uuid, template_id, template_path) VALUES (?, ?, ?);",
            uuid,
            "défaut",
            "static/requisomatic_base_template.odt",
        )
        return redirect(f"/{uuid}")
    elif state == "add":
        return render_template(
            "add.html",
            uuid=uuid,
            breadcrumbs=breadcrumbs(state, uuid),
            footer=Markup(render_template("footer.html", version=__version__)),
        )
    elif state == "whois":
        nb_whoised = sql1(
            """SELECT COUNT(*) FROM whois, ip_addresses
        WHERE ip_addresses.uuid=? AND whois.ip=ip_addresses.ip AND whois.whois IS NOT NULL;""",
            uuid,
        )
        nb_total = sql1("""SELECT COUNT(*) FROM ip_addresses WHERE uuid=?;""", uuid)
        progress = nb_whoised / nb_total * 100 if nb_total > 0 else 100
        if progress != 100:
            wait_time = str(
                datetime.timedelta(seconds=(nb_total - nb_whoised) * whois_delay)
            )
            return render_template(
                "whois.html",
                uuid=uuid,
                progress=progress,
                time=wait_time,
                breadcrumbs=breadcrumbs(state, uuid),
                footer=Markup(render_template("footer.html", version=__version__)),
            )
        else:
            return render_template(
                "whois.html",
                progress=progress,
                uuid=uuid,
                ip_table=whois_ip_table(uuid),
                email_table=whois_email_table(uuid),
                breadcrumbs=breadcrumbs(state, uuid),
                footer=Markup(render_template("footer.html", version=__version__)),
            )
    elif state == "contact":
        return render_template(
            "contact.html",
            uuid=uuid,
            table=contact_table(uuid),
            breadcrumbs=breadcrumbs(state, uuid),
            footer=Markup(render_template("footer.html", version=__version__)),
        )
    elif state == "template":
        return render_template(
            "template.html",
            uuid=uuid,
            table=template_table(uuid),
            breadcrumbs=breadcrumbs(state, uuid),
            footer=Markup(render_template("footer.html", version=__version__)),
        )
    elif state == "download":
        return render_template(
            "download.html",
            uuid=uuid,
            table=download_table(uuid),
            breadcrumbs=breadcrumbs(state, uuid),
            footer=Markup(render_template("footer.html", version=__version__)),
        )
    else:
        raise Exception(f"Unknown state {state}")


base_10_byte_res = "[12]*\d*\d+"
ipv4_res = (
    base_10_byte_res
    + "\."
    + base_10_byte_res
    + "\."
    + base_10_byte_res
    + "\."
    + base_10_byte_res
)
ipv4_re = re.compile(ipv4_res)


def extract_ip_addresses_and_comments(s):
    """Extract all IP addresses and associated comments from the given strings

    Lines with no IP addresses are skipped.

    The comments are the part of a line which are not the IP address."""
    ips = []
    comments = []
    for line in s.split("\n"):
        if ip_match := ipv4_re.search(line):
            ips.append(ip := ip_match.group(0))
            comments.append(line.replace(ip, "").strip())
    return ips, comments


email_re = re.compile(r'[^" \\]+@[^" \\]+')


def extract_email_addresses_and_comments(s):
    """Extract all email addresses and associated comments from the given strings

    Lines with no email addresses are skipped.

    The comments are the part of a line which are not the email address."""
    emails = []
    comments = []
    for line in s.split("\n"):
        if email_match := email_re.search(line):
            emails.append(email := email_match.group(0).strip())
            comments.append(line.replace(email, "").strip())
    return emails, comments


def switch_state(uuid, state):
    """Set uuid's state to state and redirect to /"""
    sql_commit("""UPDATE state SET state = ? WHERE uuid = ?;""", state, uuid)
    return redirect(f"/{uuid}", code=303)


@app.route("/<uuid>/add", methods=["POST", "GET"])
def _add(uuid):
    """Parse the given text, add the IP and email addresses to the appropriate tables, and switch to whois state.

    IPs are added to the table of IPs to run whois on if necessary.

    They are also added to the table of IP to subpoena for the current UUID.

    Emails are only added to the table of emails to subpoena for the current UUID."""
    if request.method == "GET":
        return switch_state(uuid, "add")
    assert request.method == "POST", f"Unexpected method {request.method}"

    ip_addresses, comments = extract_ip_addresses_and_comments(
        request.values["addresses"]
    )
    for ip, comment in zip(ip_addresses, comments):
        # Add the ip and comment to the table of IPs to subpoena, in relation to the active uuid
        sql_commit(
            """INSERT INTO ip_addresses (uuid, ip, comment) VALUES (?, ?, ?)
        ON CONFLICT(uuid, ip) DO UPDATE SET comment = ?;""",
            uuid,
            ip,
            comment,
            comment,
        )
        known = sqll("""SELECT 1 FROM whois WHERE ip = ?;""", ip)
        if not known:
            # Add the IP to the table of IP to fetch the whois of
            sql_commit(
                """INSERT INTO whois (ip, whois, date) VALUES (?, ?, ?);""",
                ip,
                None,
                None,
            )

    emails, comments = extract_email_addresses_and_comments(request.values["addresses"])
    for email, comment in zip(emails, comments):
        # Add the email to the table of emails to subpoena, in relation to the active uuid
        sql_commit(
            """INSERT INTO email_addresses (uuid, email, comment) VALUES (?, ?, ?)
        ON CONFLICT(uuid, email) DO UPDATE SET comment = ?;""",
            uuid,
            email,
            comment,
            comment,
        )
    return switch_state(uuid, "whois")


@app.route("/whois/<ip>")
def _whois_raw(ip):
    """Display the syntax colored whois results"""
    answer = json.loads(sql1("""SELECT whois FROM whois WHERE ip=?;""", ip))
    return highlight(pprint.pformat(answer), PythonLexer(), HtmlFormatter(full=True))


@app.route("/<uuid>/whois/email", methods=["POST"])
def _whois_email(uuid):
    """Select the given hosting providers as the one to use for the given uuid's
    email domains.
    """
    for hosting_provider_string in [
        s for s in request.values.keys() if s.startswith("hosting_provider_")
    ]:
        domain = hosting_provider_string[len("hosting_provider_") :]
        # If the hosting provider was not selected via a fixed value radio button
        if (hosting_provider := request.values[hosting_provider_string]) == "_custom":
            # get the value from the associated text field
            hosting_provider = request.values[f"custom_{hosting_provider_string}"]
        escaped_domain = domain.replace(
            "_", "\_"
        )  # _ means 'any character' in for SQL's LIKE operator
        assert (
            sql1ao1(
                """SELECT COUNT(*) FROM email_addresses WHERE uuid = ? AND email LIKE ?;""",
                uuid,
                f"%@{escaped_domain}",
            )
            >= 1
        ), f"User {uuid} is not recorded as having given us any email whose domain is {domain} !"
        sql_commit(
            """UPDATE email_addresses SET hosting_provider = ? WHERE uuid = ? AND email LIKE ?;""",
            hosting_provider,
            uuid,
            f"%@{escaped_domain}",
        )
    return switch_state(uuid, "contact")


@app.route("/<uuid>/whois/ip", methods=["POST"])
def _whois_ip(uuid):
    """Select the given hosting providers as the one to use for the given uuid's
    IP addresses.
    """
    for hosting_provider_string in [
        s for s in request.values.keys() if s.startswith("hosting_provider_")
    ]:
        ip = hosting_provider_string[len("hosting_provider_") :]
        # If the hosting provider was not selected via a fixed value radio button
        if (hosting_provider := request.values[hosting_provider_string]) == "_custom":
            # get the value from the associated text field
            hosting_provider = request.values[f"custom_{hosting_provider_string}"]
        assert (
            len(
                db.execute(
                    """SELECT hosting_provider FROM ip_addresses WHERE uuid = ? AND ip = ?;""",
                    (uuid, ip),
                ).fetchall()
            )
            == 1
        ), f"User {uuid} is not recorded as having given us ip {ip} !"
        sql_commit(
            """UPDATE ip_addresses SET hosting_provider = ? WHERE uuid = ? AND ip = ?;""",
            hosting_provider,
            uuid,
            ip,
        )
    return switch_state(uuid, "contact")


@app.route("/<uuid>/whois")
def _whois(uuid):
    """Switch back to whois state. This request happens when a user clicks on a breadcrumb."""
    return switch_state(uuid, "whois")


@app.route("/<uuid>/contact", methods=["POST", "GET"])
def _contact(uuid):
    """Select the given contacts as the one to use for the current uuid.

    New contacts are saved for every user for later reuse."""
    if request.method == "GET":
        return switch_state(uuid, "contact")
    assert request.method == "POST", f"Unexpected method {request.method}"
    for contact_string in [
        s for s in request.values.keys() if s.startswith("contact_")
    ]:
        hosting_provider = contact_string[len("contact_") :]
        assert (
            sql1ao1(
                """SELECT COUNT(*) FROM ip_addresses WHERE uuid=? AND hosting_provider=?;""",
                uuid,
                hosting_provider,
            )
            + sql1ao1(
                """SELECT COUNT(*) FROM email_addresses WHERE uuid=? AND hosting_provider=?;""",
                uuid,
                hosting_provider,
            )
            >= 1
        ), f"User {uuid} is not trying to subpoena hosting provider {hosting_provider}."
        # If the contact info was not selected via a fixed value radio button
        if (contact_id := request.values[contact_string]) == "_custom":
            # get the value from the associated text field
            contact_info = request.values[f"custom_{contact_string}"]
            # Add the newly provided value to the database
            # Find the incremental id
            last_index = max(
                [
                    int(contact_id.split("_")[-1])
                    for contact_id in sqll(
                        "SELECT contact_id FROM address_book WHERE hosting_provider = ?;",
                        hosting_provider,
                    )
                ],
                default=0,
            )
            contact_id = f"{hosting_provider}_{last_index+1}"
            # Insert the new contact info
            sql_commit(
                """INSERT INTO address_book (contact_id, hosting_provider, contact) VALUES (?, ?, ?);""",
                contact_id,
                hosting_provider,
                contact_info,
            )
        else:
            # Get the value from the database
            contact_info = sql1(
                """SELECT contact FROM address_book WHERE contact_id = ?;""", contact_id
            )
        sql_commit(
            """INSERT INTO provider_contact (uuid, hosting_provider, contact) VALUES (?, ?, ?)
        ON CONFLICT(uuid, hosting_provider) DO UPDATE SET contact=excluded.contact;""",
            uuid,
            hosting_provider,
            contact_info,
        )
    return switch_state(uuid, "template")


@app.route("/<uuid>/new_template", methods=["POST"])
def _new_template(uuid):
    """Save a new template on disk and in the DB."""
    # check if the post request has the file part
    new_template = request.files["new_template_file"]
    # if user does not select file, browser also
    # submit an empty part without filename
    if new_template.filename == "":
        raise ValueError("No file selected")
    fname = secure_filename(request.values["new_template_name"])
    new_template.save(os.path.join(TMPDIR, fname))
    print(f"File saved in {os.path.join(TMPDIR, fname)}")
    sql_commit(
        """INSERT INTO template (uuid, template_id, template_path) VALUES (?, ?, ?)
    ON CONFLICT(uuid, template_id) DO UPDATE SET template_path=excluded.template_path;""",
        uuid,
        fname,
        os.path.join(TMPDIR, fname),
    )
    return redirect(f"/{uuid}", code=303)


@app.route("/<uuid>/template", methods=["POST", "GET"])
def _template(uuid):
    """Save in the DB the user's choice about which template to use, then switch to state download."""
    if request.method == "GET":
        return switch_state(uuid, "template")
    assert request.method == "POST", f"Unexpected method {request.method}"
    for template_string in [
        s for s in request.values.keys() if s.startswith("template_")
    ]:
        hosting_provider = template_string[len("template_") :]
        assert (
            sql1ao1(
                """SELECT COUNT(*) FROM ip_addresses WHERE uuid=? AND hosting_provider=?;""",
                uuid,
                hosting_provider,
            )
            + sql1ao1(
                """SELECT COUNT(*) FROM email_addresses WHERE uuid=? AND hosting_provider=?;""",
                uuid,
                hosting_provider,
            )
            >= 1
        ), f"User {uuid} is not trying to subpoena hosting provider {hosting_provider}."
        template_id = request.values[template_string]
        sql_commit(
            """INSERT INTO provider_template (uuid, hosting_provider, template_id) VALUES (?, ?, ?)
        ON CONFLICT(uuid, hosting_provider) DO UPDATE SET template_id=excluded.template_id;""",
            uuid,
            hosting_provider,
            template_id,
        )
    return switch_state(uuid, "download")


@app.route("/<uuid>/download/<fname>")
def _download(uuid, fname):
    """Serve the file for the requested hosting provider."""
    assert fname.endswith(".odt")
    hosting_provider = fname[: -len(".odt")]
    template_id = sql1ao1(
        """SELECT template_id FROM provider_template WHERE uuid=? AND hosting_provider=?;""",
        uuid,
        hosting_provider,
    )
    template_path = sql1ao1(
        """SELECT template_path FROM template WHERE uuid=? AND template_id=?;""",
        uuid,
        template_id,
    )
    contact_info = sql1ao1(
        """SELECT contact FROM provider_contact WHERE uuid=? AND hosting_provider=?;""",
        uuid,
        hosting_provider,
    )
    ip_addresses = sqll(
        """SELECT ip FROM ip_addresses WHERE uuid = ? and hosting_provider = ?;""",
        uuid,
        hosting_provider,
    )
    comments = [
        sql1ao1(
            """SELECT comment FROM ip_addresses WHERE ip = ? AND uuid = ?;""", ip, uuid
        )
        for ip in ip_addresses
    ]
    email_addresses = sqll(
        """SELECT email FROM email_addresses WHERE uuid=? and hosting_provider=?;""",
        uuid,
        hosting_provider,
    )
    comments += [
        sql1ao1(
            """SELECT comment FROM email_addresses WHERE email = ? AND uuid=?;""",
            email,
            uuid,
        )
        for email in email_addresses
    ]
    addresses = ip_addresses + email_addresses
    subpoena_data = secretary_engine.render(
        template_path,
        contact=contact_info,
        ipcs=[
            {"ip": address, "c": comment}
            for address, comment in zip(addresses, comments)
        ],
    )
    return Response(subpoena_data, mimetype="application/vnd.oasis.opendocument.text")
